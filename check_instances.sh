echo "Instance Name:"
aws --profile its ec2 describe-instances --region eu-west-1 --query 'Reservations[*].Instances[*].[InstanceId]' --filters Name=instance-state-name,Values=running --output text | xargs -n1 aws --profile its ec2 describe-instances --region eu-west-1 --instance-id | jq .Reservations[0].Instances[0].Tags[0].Value
echo "Instance Id:"
aws --profile its ec2 describe-instances --region eu-west-1 --query 'Reservations[*].Instances[*].[InstanceId]' --filters Name=instance-state-name,Values=running --output text | xargs -n1 aws --profile its ec2 describe-instances --region eu-west-1 --instance-id | jq .Reservations[0].Instances[0].InstanceId

