# staging
scp \
    ./server/.env \
    steva01:~/photocontest/backend

# production
scp \
    -i '/home/marco/project/ITS/2°ANNO/Architetture Cloud/photocontest/scripts/ec2-stevanon.pem' \
    ./server/.env \
    ubuntu@ec2-63-33-232-170.eu-west-1.compute.amazonaws.com:~/apps/ITS_PhotoContest-Backend